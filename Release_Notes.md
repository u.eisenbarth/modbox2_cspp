Release Notes for the modbox2_cspp Project
===========================================
This LabVIEW project _modbox2_cspp.lvproj_ is used to develop the applications based on NI ActorFramework and CS++ libraries.

Version 0.0.0.0 25-05-2020 u.eisenbarth@gsi.de
--------------------------------------
The modbox2_cspp project was just forked. There is the master branch with some submodules, only.

